# closeclassroom

## Architecture & micro-services
L'application possède deux services. Un premier service nommé `utilisateur_service` permet de gérer les utilisateurs. Un deuxième service nommé `cours_service` est lui chargé de gérer les cours et les épisodes.

Les administrateurs utilisent les mêmes routes que les routes publiques, seulement, celles ci gèrent les actions administrateur différemment. J'ai choisi de faire cela car c'est plus logique pour moi de créer une route par fonctionnalité, et ensuite de gérer différement les événements en fonction de l'utilisateur qui utilise la route.

L'idée est qu'il y ai par là dessus un front en single page application comme VueJS ou Angular, et que ce front utilise les différents services en communiquant à la bonne adresse suivant ce qu'il veut faire.

## Dockerisation
L'application est entièrement dockerisée, ce qui est très pratique pour le déploiement et l'aspect micro service.
En tout, il y a 3 containers: un pour la base de donnée (`mariadb`), un pour le service utilisateur et le dernier pour le service cours.

J'ai également créer un fichier `docker-compose.yml` pour faciliter la gestion des différents containers.

## Développement
Cette partie permet de build & démarrer manuellement les différentes instances de l'application. Pour une utilisation basique, passez directement à la partie _Utilisation_.

L'application est réalisée à base de module spring ce qui permet de build toute l'application grâce à un seul `mvn clean package -DskipTests` dans le répertoire racine.
Ensuite, il faut quand même démarrer individuellement chaque fichier `.jar` sans oublier de donner les variables d'environnement requises (cf `application.properties`)

## Utilisation
Avant tout, procédez à un `mvn clean package -DskipTests` à la racine du projet pour compiler l'application java.
Ensuite, pour démarrer l'application, il vous suffit de faire un `docker-compose up`.
Le service utilisateur sera accessible sur le port `8080` et le service cours sera accessible sur le port `8081`.

Avant toute chose, certaines routes nécéssitent une authentification afin de vérifier si tel ou tel utilisateur à la permission d'effectuer une action. Comme demandé, je n'ai pas poussé le projet jusqu'à faire un système d'authentification. Du coup, pour simuler une authentification, il faut simplement mettre l'id de l'utilisateur dans le header `Authorization` de la requête. Pour simuler une connection administrateur, il faut simplement mettre `admin` dans le header `Authorization`.

Toute l'API recoit et renvoie du JSON. Le tout respecte également l'architecture HATEOAS comme demandé.

## Description des routes
Pour plus de facilité, j'ai partagé une collection Postman qui contient toutes les requêtes. Pour y accéder cliquez sur le bouton ci-dessous.

[![cliquez ici](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/c75331f176959ce93c02) 

### Service utilisateur
#### Récupérer tous les utilisateurs
GET `http://localhost:8080/utilisateurs`

Paramètres possibles: 
- `prenom`: N'afficher que les utilisateurs ayant ce prénom
- `nom`: N'afficher que les utilisateurs ayant ce nom
- `supprime`: Si `true`, n'afficher que les utilisateurs supprimés. Si `false`, n'afficher que les utilisateurs non supprimés

Réponses possibles:
- Status `OK`: Retourne le tableau des utilisateurs correspondants à la recherche

#### Récupérer un utilisateur
GET `http://localhost:8080/utilisateurs/{id}`

Valeurs de l'URL:
- `id`: id de l'utilisateur à récupérer

Réponses possibles:
- Status `OK`: Retourne l'utilisateur correspondant
- Status `NOT_FOUND`: L'utilisateur n'as pas été trouvé

#### Créer un utilisateur
POST `http://localhost:8080/utilisateurs`

Body attendu: 
- `email`: Email de l'utilsateur
- `prenom`: Prenom de l'utilisateur
- `nom`: Nom de l'utilisateur

Réponses possibles:
- Status `CREATED`: L'utilisateur à bien été créé
- Status `BAD_REQUEST`: Mauvais format des données du body ou un utilisateur existe déjà avec cet email

#### Modifier un utilisateur
_Nécessite une authentification_

PUT `http://localhost:8080/utilisateurs/{id}`

Valeurs de l'URL:
- `id`: id de l'utilisateur à modifier

Body attendu: 
- `email`: Email de l'utilsateur
- `prenom`: Prenom de l'utilisateur
- `nom`: Nom de l'utilisateur

Réponses possibles:
- Status `NO_CONTENT`: L'utilisateur à bien été modifié
- Status `UNAUTHORIZED`: L'utilisateur connecté ne peux pas effectuer cette action
- Status `BAD_REQUEST`: Mauvais format des données du body ou un utilisateur existe déjà avec cet email

#### Supprimer un utilisateur
_Nécessite une authentification_

DELETE `http://localhost:8080/utilisateurs/{id}`

Valeurs de l'URL:
- `id`: id de l'utilisateur à supprimer

Réponses possibles:
- Status `NO_CONTENT`: L'utilisateur à bien été supprimé
- Status `UNAUTHORIZED`: L'utilisateur connecté ne peux pas effectuer cette action
- Status `BAD_REQUEST`: Mauvais format des données du body ou un utilisateur existe déjà avec cet email

#### Récupérer les cours d'un utilisateur
GET `http://localhost:8080/utilisateurs/{id}/cours`

Valeurs de l'URL:
- `id`: id de l'utilisateur

Redirection vers `http://localhost:8081/cours?utilisateur={id}`

#### Récupérer les épisodes visionnés d'un utilisateur
GET `http://localhost:8080/utilisateurs/{id}/episodes-visionnes`

Valeurs de l'URL:
- `id`: id de l'utilisateur

Redirection vers `http://localhost:8081/episodes?visionnePar={id}`

### Inscription à un cours
_Nécessite une authentification_

POST `http://localhost:8080/utilisateurs/{id}/cours/{coursID}`

Valeurs de l'URL:
- `id`: id de l'utilisateur à inscrire au cours
- `coursID`: id du cours auquel inscrire l'utilisateur

Body attendu: 
- `numeroCarte`: Numéro de la CB (`XXXX`)
- `code`: Code de la carte (`XXXX`)
- `codeValidation`: Code validation (`XXXX`)

Réponses possibles:
- Status `NO_CONTENT`: L'utilisateur à bien été inscrit au cours
- Status `NOT_FOUND`: L'utilisateur ou le cours n'as pas été trouvé
- Status `BAD_REQUEST`: Oubli du header `Authorization`, mauvais format de body ou données de paiement invalides
- Status `UNAUTHORIZED`: L'utilisateur connecté ne peux pas effectuer cette action

### Désinscription à un cours
_Nécessite une authentification_

DELETE `http://localhost:8080/utilisateurs/{id}/cours/{coursID}`

Valeurs de l'URL:
- `id`: id de l'utilisateur à désinscrire du cours
- `coursID`: id du cours auquel déinscrire l'utilisateur

Réponses possibles:
- Status `NO_CONTENT`: L'utilisateur à bien été désinscrit du cours
- Status `NOT_FOUND`: L'utilisateur n'as pas été trouvé
- Status `BAD_REQUEST`: Oubli du header `Authorization` ou utilisateur non inscrit au cours
- Status `UNAUTHORIZED`: L'utilisateur connecté ne peux pas effectuer cette action

### Visionnage d'un épisode
PUT `http://localhost:8080/utilisateurs/{id}/cours/{coursID}`

Valeurs de l'URL:
- `id`: id de l'utilisateur à désinscrire du cours
- `coursID`: id du cours auquel déinscrire l'utilisateur

Réponses possibles:
- Status `NO_CONTENT`: L'épisode à bien été à la liste des épisodes visionés par l'utilisateur
- Status `NOT_FOUND`: L'utilisateur n'as pas été trouvé
- Status `UNAUTHORIZED`: L'utilisateur connecté ne peux pas effectuer cette action

### Service cours & épisodes
#### Récupérer tous les cours
GET `http://localhost:8081/cours`

Paramètres possibles: 
- `prixMin`: N'afficher que les cours avec un prix au dessus de ce paramètre
- `prixMax`: N'afficher que les cours avec un prix en dessous de ce paramètre
- `utilisateur`: N'afficher que les cours possédés par l'utilisateur ayant ce paramètre comme id
- `theme`: N'afficher que les cours avec le thème en paramètre

Réponses possibles:
- Status `OK`: Retourne le tableau des cours correspondants à la recherche

#### Récupérer les épisodes d'un cours
GET `http://localhost:8081/cours/{id}/episodes`

Valeurs de l'URL:
- `id`: id du cours

Redirection vers `http://localhost:8081/episodes?cours={id}`

#### Créer un cours
_Nécessite une authentification administrateur_

POST `http://localhost:8081/cours`

Body attendu: 
- `nom`: Nom du cours
- `description`: Description du cours
- `theme`: Thème du cours
- `prix`: Prix du cours

Réponses possibles:
- Status `CREATED`: Le cours à bien été créé
- Status `UNAUTHORIZED`: L'utilisateur connecté ne peux pas effectuer cette action
- Status `BAD_REQUEST`: Mauvais format des données du body

#### Modifier un cours
_Nécessite une authentification administrateur_

PUT `http://localhost:8081/cours/{id}`

Valeurs de l'URL:
- `id`: id du cours à modifier

Body attendu: 
- `nom`: Nom du cours
- `description`: Description du cours
- `theme`: Thème du cours
- `prix`: Prix du cours

Réponses possibles:
- Status `NO_CONTENT`: Le cours à bien été modifié
- Status `UNAUTHORIZED`: L'utilisateur connecté ne peux pas effectuer cette action
- Status `BAD_REQUEST`: Mauvais format des données du body

#### Abonnement à un cours
_Nécessite une authentification_
POST `http://localhost:8081/cours/{id}/abonnement`

Redirection vers `http://localhost:8080/utilisateurs/{utilisateurID}/cours/{id}`

#### Désabonnement d'un cours
_Nécessite une authentification_
DELETE `http://localhost:8081/cours/{id}/abonnement`

Redirection vers `http://localhost:8080/utilisateurs/{utilisateurID}/cours/{id}`



#### Récupérer tous les épisodes (sans contenu)
GET `http://localhost:8081/episodes`

Paramètres possibles: 
- `cours`: N'afficher que les episodes d'un cours particuliers
- `visionnePar`: N'afficher que les épisodes visionés par l'utilisateur ayant ce paramètre comme id

Réponses possibles:
- Status `OK`: Retourne le tableau des épisodes correspondants à la recherche. Attention, les épisodes sont retourné sans leurs contenu, car cette fonction permet juste d'avoir une idée des épisodes. Pour visionner le contenu d'un épisode il faut GET un épisode spécifique.

#### Récupérer un épisode (avec contenu)
_Nécessite une authentification_

GET `http://localhost:8081/episodes/{id}`

Valeurs de l'URL:
- `id`: id de l'épisode à récupérer

Réponses possibles:
- Status `OK`: Retourne l'épisode avec son contenu
- Status `NOT_FOUND`: L'épisode n'as pas été trouvé
- Status `PAYMENT_REQUIRED`: L'utilisateur n'est pas abonné au cours et ne peux pas accéder à l'épisode

#### Créer un épisode
_Nécessite une authentification administrateur_

POST `http://localhost:8081/episodes`

Body attendu: 
- `nom`: Nom de l'épisode
- `lien`: Lien du contenu de l'épisode
- `coursID`: ID du cours de l'épisode

Réponses possibles:
- Status `CREATED`: Le cours à bien été créé
- Status `UNAUTHORIZED`: L'utilisateur connecté ne peux pas effectuer cette action
- Status `BAD_REQUEST`: Mauvais format des données du body

#### Modifier un épisode
_Nécessite une authentification administrateur_

PUT `http://localhost:8081/episodes/{id}`

Valeurs de l'URL:
- `id`: id de l'épisode à modifier

Body attendu: 
- `nom`: Nom de l'épisode
- `lien`: Lien du contenu de l'épisode
- `coursID`: ID du cours de l'épisode

Réponses possibles:
- Status `NO_CONTENT`: L à bien été modifié
- Status `UNAUTHORIZED`: L'utilisateur connecté ne peux pas effectuer cette action
- Status `NOT_FOUND`: L'épisode n'as pas été trouvé
- Status `BAD_REQUEST`: Mauvais format des données du body