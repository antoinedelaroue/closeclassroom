package fr.miage.closeclassroom.utilisateur_service.controllers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import fr.miage.closeclassroom.common.Routes;
import fr.miage.closeclassroom.common.Services;
import fr.miage.closeclassroom.common.models.Achat;
import fr.miage.closeclassroom.common.models.Cours;
import fr.miage.closeclassroom.common.models.Episode;
import fr.miage.closeclassroom.common.models.Utilisateur;
import fr.miage.closeclassroom.utilisateur_service.repositories.UtilisateurRepository;
import fr.miage.closeclassroom.utilisateur_service.repositories.UtilisateurSpecifications;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping(value = "/utilisateurs")
@AllArgsConstructor
public class UtilisateurController {

	private final UtilisateurRepository repository;
	private final Services services;

	@PersistenceContext
	EntityManager entityManager;

	@GetMapping
	public ResponseEntity<?> findAll(@RequestParam(value = "prenom", required = false) Optional<String> prenom,
			@RequestParam(value = "nom", required = false) Optional<String> nom,
			@RequestParam(value = "supprime", required = false) Optional<Boolean> supprime) {
		Specification<Utilisateur> criteres = Specification.where(null);
		if (prenom.isPresent()) {
			criteres = criteres.and(UtilisateurSpecifications.hasPrenom(prenom.get()));
		}
		if (nom.isPresent()) {
			criteres = criteres.and(UtilisateurSpecifications.hasNom(nom.get()));
		}
		if (supprime.isPresent()) {
			criteres = criteres.and(UtilisateurSpecifications.isSupprime(supprime.get()));
		}

		List<EntityModel<Utilisateur>> utilisateurs = StreamSupport
				.stream(repository.findAll(criteres).spliterator(), false)
				.map(utilisateur -> EntityModel.of(utilisateur,
						linkTo(methodOn(UtilisateurController.class).findOne(utilisateur.getId())).withSelfRel(),
						linkTo(methodOn(UtilisateurController.class).findCours(utilisateur.getId(), null))
								.withRel("cours")))
				.collect(Collectors.toList());

		return ResponseEntity.ok(CollectionModel.of(utilisateurs,
				linkTo(methodOn(UtilisateurController.class).findAll(prenom, nom, supprime)).withSelfRel()));
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> findOne(@PathVariable Long id) {
		return repository.findById(id).map(utilisateur -> EntityModel.of(utilisateur,
				linkTo(methodOn(UtilisateurController.class).findOne(utilisateur.getId())).withSelfRel(),
				linkTo(methodOn(UtilisateurController.class).findAll(null, null, null)).withRel("utilisateurs"),
				linkTo(methodOn(UtilisateurController.class).findCours(utilisateur.getId(), null)).withRel("cours")))
				.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
	}

	@GetMapping("/{id}/cours")
	public ResponseEntity<?> findCours(@PathVariable Long id, RedirectAttributes attributes) {
		return new RestTemplate().exchange(services.getCoursAddress() + "/cours?utilisateur={id}", HttpMethod.GET, null, String.class, id);
	}

	@PostMapping("/{id}/cours/{coursID}")
	public ResponseEntity<?> inscription(@RequestHeader("Authorization") String authorization, @PathVariable Long id,
			@PathVariable Long coursID, @RequestBody Achat achat) {

		Cours cours = entityManager.getReference(Cours.class, coursID);

		Optional<Utilisateur> optionalUtilisateur = repository.findById(id);
		if (!optionalUtilisateur.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		Utilisateur utilisateur = optionalUtilisateur.get();

		if (!authorization.equals(utilisateur.getId().toString()) && !authorization.equals("admin")) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

		// verification si le cours est déjà acheté
		if (utilisateur.possedeCours(coursID)) {
			return ResponseEntity.badRequest().body("Vous êtes déjà inscrit à ce cours");
		}

		// verification des donnée
		if (!authorization.equals("admin") && cours.estPayant() && (achat.getNumeroCarte() == null
				|| achat.getNumeroCarte() == null || achat.getNumeroCarte() == null)) {
			return ResponseEntity.badRequest().body(
					"Mauvaise données fournies. Format requis: { numeroCarte: string, code: string, codeValidation: string }");
		}

		// verification des codes
		if (!authorization.equals("admin") && (!achat.getNumeroCarte().equals("XXXX") || !achat.getCode().equals("XXXX")
				|| !achat.getCodeValidation().equals("XXXX"))) {
			return ResponseEntity.badRequest().body("Données de paiement invalides");
		}

		// ajout du cours à l'utilisateurs
		utilisateur.ajouterCours(cours);

		repository.save(utilisateur);

		return ResponseEntity.noContent().build();
	}

	@DeleteMapping("/{id}/cours/{coursID}")
	public ResponseEntity<?> desinscription(@RequestHeader("Authorization") String authorization, @PathVariable Long id,
			@PathVariable Long coursID) {

		Optional<Utilisateur> optionalUtilisateur = repository.findById(id);
		if (!optionalUtilisateur.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		Utilisateur utilisateur = optionalUtilisateur.get();

		if (!authorization.equals(utilisateur.getId().toString()) && !authorization.equals("admin")) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

		if (!utilisateur.possedeCours(coursID)) {
			return ResponseEntity.badRequest().body("L'utilisateur n'est pas inscrit à ce cours");
		}

		utilisateur.supprimerCours(coursID);
		repository.save(utilisateur);

		return ResponseEntity.noContent().build();
	}

	@GetMapping("/{id}/episodes-visionnes")
	public ResponseEntity<?> findEpisodeVisionnes(@PathVariable Long id, RedirectAttributes attributes) {
		return new RestTemplate().exchange(services.getCoursAddress() + "/episodes?visionnePar={id}", HttpMethod.GET, null, String.class, id);
	}

	@PutMapping("/{id}/episodes/{episodeID}")
	public ResponseEntity<?> visionner(@RequestHeader("Authorization") String authorization, @PathVariable Long id,
			@PathVariable Long episodeID) {

		Optional<Utilisateur> optionalUtilisateur = repository.findById(id);
		if (!optionalUtilisateur.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		Utilisateur utilisateur = optionalUtilisateur.get();

		if (!authorization.equals(utilisateur.getId().toString()) && !authorization.equals("admin")) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

		// ajout de l'épisode à la liste d'épisode visionnés de l'utilisateur
		Episode episode = entityManager.getReference(Episode.class, episodeID);

		utilisateur.ajouterEpisodeVisionne(episode);
		repository.save(utilisateur);

		return ResponseEntity.noContent().build();
	}

	@PostMapping
	public ResponseEntity<?> create(@RequestBody Utilisateur utilisateur) {
		try {
			Optional<Utilisateur> existingUtilisateur = repository.findByEmail(utilisateur.getEmail());
			if (existingUtilisateur.isPresent()) {
				return ResponseEntity.badRequest().body("Un utilisateur existe déjà avec cet email");
			}

			Utilisateur saved = repository.save(utilisateur);

			EntityModel<Utilisateur> resource = EntityModel.of(saved,
					linkTo(methodOn(UtilisateurController.class).findOne(saved.getId())).withSelfRel());

			return ResponseEntity.created(new URI(resource.getRequiredLink(IanaLinkRelations.SELF).getHref()))
					.body(resource);
		} catch (URISyntaxException e) {
			return ResponseEntity.badRequest().body("Unable to create " + utilisateur);
		}
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> modify(@RequestHeader("Authorization") String authorization, @PathVariable Long id,
			@RequestBody Utilisateur newUtilisateur) {
		if (!authorization.equals("admin") && !authorization.equals(id.toString())) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

		Optional<Utilisateur> optionalUtilisateur = repository.findById(id);
		if (!optionalUtilisateur.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		Utilisateur utilisateur = optionalUtilisateur.get();

		Optional<Utilisateur> existingUtilisateur = repository.findByEmail(newUtilisateur.getEmail());
		if (existingUtilisateur.isPresent() && existingUtilisateur.get().getId() != utilisateur.getId()) {
			return ResponseEntity.badRequest().body("Un utilisateur existe déjà avec cet email");
		}

		utilisateur.setEmail(newUtilisateur.getEmail());
		utilisateur.setPrenom(newUtilisateur.getPrenom());
		utilisateur.setNom(newUtilisateur.getNom());

		repository.save(utilisateur);

		return ResponseEntity.noContent().build();
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@RequestHeader("Authorization") String authorization, @PathVariable Long id) {
		if (!authorization.equals("admin") && !authorization.equals(id.toString())) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

		Optional<Utilisateur> optionalUtilisateur = repository.findById(id);
		if (!optionalUtilisateur.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		Utilisateur utilisateur = optionalUtilisateur.get();

		utilisateur.setSupprime(true);
		repository.save(utilisateur);

		return ResponseEntity.noContent().build();
	}
}