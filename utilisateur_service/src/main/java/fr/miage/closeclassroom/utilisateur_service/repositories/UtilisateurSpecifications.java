package fr.miage.closeclassroom.utilisateur_service.repositories;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import fr.miage.closeclassroom.common.models.Utilisateur;

public class UtilisateurSpecifications {

    public static Specification<Utilisateur> hasPrenom(String prenom) {
        return new Specification<Utilisateur>() {
            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<Utilisateur> root, CriteriaQuery<?> query,
                    CriteriaBuilder criteriaBuilder) {

                return criteriaBuilder.like(criteriaBuilder.upper(root.get("prenom")),
                        "%" + prenom.toUpperCase() + "%");
            }
        };
    }

    public static Specification<Utilisateur> hasNom(String nom) {
        return new Specification<Utilisateur>() {
            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<Utilisateur> root, CriteriaQuery<?> query,
                    CriteriaBuilder criteriaBuilder) {

                return criteriaBuilder.like(criteriaBuilder.upper(root.get("nom")),
                        "%" + nom.toUpperCase() + "%");
            }
        };
    }

    public static Specification<Utilisateur> isSupprime(boolean supprime) {
        return new Specification<Utilisateur>() {
            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<Utilisateur> root, CriteriaQuery<?> query,
                    CriteriaBuilder criteriaBuilder) {

                return criteriaBuilder.equal(root.get("supprime"), supprime);
            }
        };
    }
}
