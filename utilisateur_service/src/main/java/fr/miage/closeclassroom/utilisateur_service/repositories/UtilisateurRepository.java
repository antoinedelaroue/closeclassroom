package fr.miage.closeclassroom.utilisateur_service.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import fr.miage.closeclassroom.common.models.Utilisateur;

@RepositoryRestResource(exported = false)
public interface UtilisateurRepository
        extends CrudRepository<Utilisateur, Long>, JpaSpecificationExecutor<Utilisateur> {
                public Optional<Utilisateur> findByEmail(String email);
}