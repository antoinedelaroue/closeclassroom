package fr.miage.closeclassroom.common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class Services {
    @Value("${services.cours.address:'http://localhost:8081'}")
    private String coursAddress;

    @Value("${services.utilisateur.address:'http://localhost:8080'}")
    private String utilisateurAddress;
}
