package fr.miage.closeclassroom.common.models;

import lombok.Data;

@Data
public class Achat {
    private String numeroCarte;
    private String code;
    private String codeValidation;
}
