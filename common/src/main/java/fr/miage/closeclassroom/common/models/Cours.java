package fr.miage.closeclassroom.common.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SequenceGenerator(name = "cours_id_seq", sequenceName = "cours_id_seq", allocationSize = 1, initialValue = 100)
public class Cours {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cours_id_seq")
    private Long id;
    private String nom;
    private String description;
    private String theme;
    private float prix;

    @JsonIgnore
    @OneToMany(mappedBy = "cours")
    private List<Episode> episodes = new ArrayList<Episode>();

    public boolean estPayant(){
        return this.prix > 0;
    }
}
