package fr.miage.closeclassroom.common.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SequenceGenerator(name = "utilisateur_id_seq", sequenceName = "utilisateur_id_seq", allocationSize = 1, initialValue = 100)
public class Utilisateur {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "utilisateur_id_seq")
    private Long id;
    private String email;
    private String nom;
    private String prenom;
    private boolean supprime = false;

    @JsonIgnore
    @ManyToMany
    private List<Cours> cours = new ArrayList<Cours>();
    
    @JsonIgnore
    @ManyToMany
    private List<Episode> episodeVisiones = new ArrayList<Episode>();

    public void ajouterCours(Cours cours){
        this.cours.add(cours);
    }

    public void supprimerCours(Long id){
        this.cours.removeIf(c -> c.getId().equals(id));
    }

    public void ajouterEpisodeVisionne(Episode episode){
        this.episodeVisiones.add(episode);
    }

    public boolean possedeCours(Long id){
        return this.cours.stream().filter(c -> c.getId().equals(id)).findFirst().isPresent();
    }
}
