package fr.miage.closeclassroom.common.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

import fr.miage.closeclassroom.common.views.EpisodeViews;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SequenceGenerator(name = "episode_id_seq", sequenceName = "episode_id_seq", allocationSize = 1, initialValue = 100)
public class Episode {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "episode_id_seq")
    @JsonView(EpisodeViews.Public.class)
    private Long id;

    @JsonView(EpisodeViews.Public.class)
    private String nom;

    @JsonView(EpisodeViews.Private.class)
    private String lien;

    @JsonIgnore
    @ManyToOne
    private Cours cours;
}
