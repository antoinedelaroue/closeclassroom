package fr.miage.closeclassroom.common;

public class Routes {
    public static final String UTILISATEURS = "/utilisateurs";
    public static final String UTILISATEUR = UTILISATEURS + "/{id}";
    public static final String UTILISATEUR_COURSES = UTILISATEUR + "/cours";
    public static final String UTILISATEUR_COURS = UTILISATEUR_COURSES + "/{coursID}";

    public static final String COURSES = "/cours";
    public static final String COURS = COURSES + "/{id}";
    public static final String COURS_EPISODES = COURS + "/episodes";

    public static final String EPISODES = "/episodes";

}
