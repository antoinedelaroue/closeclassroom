INSERT INTO cours (id, nom, description, theme, prix) VALUES (1, 'Golang', 'Cours sur le langage golang inventé par Google', 'Langages de programmation', 12.99)
INSERT INTO cours (id, nom, description, theme, prix) VALUES (2, 'Autoconstruction écologique', 'Un cours sur les différents aspects de l''autoconstruction écologique', 'Construction', 14.99)
INSERT INTO cours (id, nom, description, theme, prix) VALUES (3, 'Psychologie inversée', 'Cours passionnant pour manipuler votre entourage grâce à la pschologie inversée', 'Psychologie', 8.99)
INSERT INTO cours (id, nom, description, theme, prix) VALUES (4, 'Spring Boot', 'Cours sur le framework Spring Boot de java', 'Langages de programmation', 0)

-- ÉPISODES COURS 1
INSERT INTO episode (id, nom, lien, cours_id) VALUES (1, 'Les variables', 'https://www.youtube.com/watch?v=FI3ywfQdWqI',1)
INSERT INTO episode (id, nom, lien, cours_id) VALUES (2, 'Les boucles', 'https://www.youtube.com/watch?v=Ndfv81JLt30', 1)
INSERT INTO episode (id, nom, lien, cours_id) VALUES (3, 'Bonnes pratiques', 'https://www.youtube.com/watch?v=crvKONWSTuU', 1)

-- ÉPISODES COURS 2
INSERT INTO episode (id, nom, lien, cours_id) VALUES (4, 'Le bioclimatisme', 'https://www.youtube.com/watch?v=wjiok-MyW9w', 2)
INSERT INTO episode (id, nom, lien, cours_id) VALUES (5, 'L''isolation', 'https://www.youtube.com/watch?v=fBChUADNhEM', 2)
INSERT INTO episode (id, nom, lien, cours_id) VALUES (6, 'La masse thermique', 'https://www.youtube.com/watch?v=Lv8eZQkj3xo', 2)
INSERT INTO episode (id, nom, lien, cours_id) VALUES (7, 'L''ossature bois', 'https://www.youtube.com/watch?v=HesfGNiavqw', 2)

-- ÉPISODES COURS 3
INSERT INTO episode (id, nom, lien, cours_id) VALUES (8, 'Marketing', 'https://www.youtube.com/watch?v=4Wr7Hqh6Xow', 3)
INSERT INTO episode (id, nom, lien, cours_id) VALUES (9, 'Quelques techniques de manipulation', 'https://www.youtube.com/watch?v=tWJJ0YaPboE',3)

-- ÉPISODES COURS 4
INSERT INTO episode (id, nom, lien, cours_id) VALUES (10, 'Les routes', 'https://www.youtube.com/watch?v=C3ZrOj4unss', 4)
INSERT INTO episode (id, nom, lien, cours_id) VALUES (11, 'Hateoas', 'https://i.ytimg.com/an_webp/vvcANMpfr5I/mqdefault_6s.webp?du=3000&sqp=CJj9y_8F&rs=AOn4CLCKNcoZyi0AQKkSFkH_kNgqLW2W4A', 4)
INSERT INTO episode (id, nom, lien, cours_id) VALUES (12, 'Les projections', 'https://www.youtube.com/watch?v=jevTat_Jkgg', 4)
INSERT INTO episode (id, nom, lien, cours_id) VALUES (13, 'Bonnes pratiques', 'https://www.youtube.com/watch?v=j2TvBaN4jdE', 4)

-- COURS UTILISATEURS
INSERT INTO utilisateur_cours (utilisateur_id, cours_id) VALUES (1, 1)
INSERT INTO utilisateur_cours (utilisateur_id, cours_id) VALUES (1, 2)
INSERT INTO utilisateur_cours (utilisateur_id, cours_id) VALUES (2, 2)

