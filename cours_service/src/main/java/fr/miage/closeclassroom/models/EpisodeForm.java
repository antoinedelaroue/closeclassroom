package fr.miage.closeclassroom.models;

import fr.miage.closeclassroom.common.models.Episode;
import lombok.Data;

@Data
public class EpisodeForm {
    private String nom;
    private String lien;
    private Long coursID;
}
