package fr.miage.closeclassroom.cours_service.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import fr.miage.closeclassroom.common.models.Episode;
import fr.miage.closeclassroom.cours_service.repositories.EpisodeRepository;
import fr.miage.closeclassroom.cours_service.repositories.EpisodeSpecifications;

@Service
public class EpisodeService {

    @Autowired
    private EpisodeRepository repository;

    public List<Episode> findAll(Optional<Long> cours, Optional<Long> utilisateur) {

        Specification<Episode> criteres = Specification.where(null);

        if (cours.isPresent()) {
            criteres = criteres.and(EpisodeSpecifications.ofCours(cours.get()));
        }
        if (utilisateur.isPresent()) {
            criteres = criteres.and(EpisodeSpecifications.isUtilisateurAbonne(utilisateur.get()));
        }

        return repository.findAll(criteres);
    }
}
