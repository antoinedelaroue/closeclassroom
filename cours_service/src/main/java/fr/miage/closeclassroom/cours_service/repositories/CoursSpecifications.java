package fr.miage.closeclassroom.cours_service.repositories;

import java.util.Collection;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.data.jpa.domain.Specification;

import fr.miage.closeclassroom.common.models.Cours;
import fr.miage.closeclassroom.common.models.Utilisateur;

public class CoursSpecifications {

    public static Specification<Cours> hasPrixMin(float min) {
        return new Specification<Cours>() {
            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<Cours> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.greaterThanOrEqualTo(root.get("prix"), min);
            }
        };
    }

    public static Specification<Cours> hasPrixMax(float max) {
        return new Specification<Cours>() {
            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<Cours> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.lessThanOrEqualTo(root.get("prix"), max);
            }
        };
    }

    public static Specification<Cours> hasTheme(String theme) {
        return new Specification<Cours>() {
            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<Cours> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.like(criteriaBuilder.upper(root.get("nom")), "%" + theme.toUpperCase() + "%");
            }
        };
    }

    public static Specification<Cours> isUtilisateurAbonne(Long userID) {
        return new Specification<Cours>() {
            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<Cours> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                query.distinct(true);
                Root<Cours> cours = root;
                Subquery<Utilisateur> utilisateurSubquery = query.subquery(Utilisateur.class);
                Root<Utilisateur> utilisateur = utilisateurSubquery.from(Utilisateur.class);
                Expression<Collection<Cours>> utilisateurCours = utilisateur.get("cours");
                utilisateurSubquery.select(utilisateur);
                utilisateurSubquery.where(criteriaBuilder.equal(utilisateur.get("id"), userID),
                        criteriaBuilder.isMember(cours, utilisateurCours));
                return criteriaBuilder.exists(utilisateurSubquery);
            }
        };
    }
}
