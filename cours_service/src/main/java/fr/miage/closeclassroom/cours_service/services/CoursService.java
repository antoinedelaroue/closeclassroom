package fr.miage.closeclassroom.cours_service.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import fr.miage.closeclassroom.common.models.Cours;
import fr.miage.closeclassroom.cours_service.repositories.CoursRepository;
import fr.miage.closeclassroom.cours_service.repositories.CoursSpecifications;

@Service
public class CoursService {

    @Autowired
    private CoursRepository repository;
    
    @GetMapping
	public List<Cours> findAll(Optional<Float> prixMin,	Optional<Float> prixMax, Optional<Long> utilisateur, Optional<String> theme) {
		Specification<Cours> criteres = Specification.where(null);
		if (prixMin.isPresent()) {
			criteres = criteres.and(CoursSpecifications.hasPrixMin(prixMin.get()));
		}
		if (prixMax.isPresent()) {
			criteres = criteres.and(CoursSpecifications.hasPrixMax(prixMax.get()));
		}
		if (utilisateur.isPresent()) {
			criteres = criteres.and(CoursSpecifications.isUtilisateurAbonne(utilisateur.get()));
		}
		if (theme.isPresent()) {
			criteres = criteres.and(CoursSpecifications.hasTheme(theme.get()));
        }
        
        return repository.findAll(criteres);
    }

}
