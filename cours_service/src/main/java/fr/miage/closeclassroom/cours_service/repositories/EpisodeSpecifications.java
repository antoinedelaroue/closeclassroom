package fr.miage.closeclassroom.cours_service.repositories;

import java.util.Collection;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.data.jpa.domain.Specification;

import fr.miage.closeclassroom.common.models.Cours;
import fr.miage.closeclassroom.common.models.Episode;
import fr.miage.closeclassroom.common.models.Utilisateur;

public class EpisodeSpecifications {

    public static Specification<Episode> ofCours(Long coursID) {
        return new Specification<Episode>() {
            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<Episode> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                Join<Episode, Cours> cours = root.join("cours");
                return criteriaBuilder.equal(cours.get("id"), coursID);
            }
        };
    }

    public static Specification<Episode> isUtilisateurAbonne(Long userID) {
        return new Specification<Episode>() {
            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<Episode> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                query.distinct(true);
                Root<Episode> episode = root;
                Subquery<Utilisateur> utilisateurSubquery = query.subquery(Utilisateur.class);
                Root<Utilisateur> utilisateur = utilisateurSubquery.from(Utilisateur.class);
                Expression<Collection<Episode>> utilisateurEpisodeVisiones = utilisateur.get("episodeVisiones");
                utilisateurSubquery.select(utilisateur);
                utilisateurSubquery.where(criteriaBuilder.equal(utilisateur.get("id"), userID), criteriaBuilder.isMember(episode, utilisateurEpisodeVisiones));
                return criteriaBuilder.exists(utilisateurSubquery);
            }
        };
    }
}
