package fr.miage.closeclassroom.cours_service.controllers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.fasterxml.jackson.annotation.JsonView;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import fr.miage.closeclassroom.common.Services;
import fr.miage.closeclassroom.common.models.Cours;
import fr.miage.closeclassroom.common.models.Episode;
import fr.miage.closeclassroom.common.views.EpisodeViews;
import fr.miage.closeclassroom.cours_service.repositories.EpisodeRepository;
import fr.miage.closeclassroom.cours_service.services.CoursService;
import fr.miage.closeclassroom.cours_service.services.EpisodeService;
import fr.miage.closeclassroom.models.EpisodeForm;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping(value = "/episodes")
@AllArgsConstructor
public class EpisodeController {

	@PersistenceContext
	EntityManager entityManager;

	private final EpisodeRepository repository;
	private final CoursService coursService;
	private final EpisodeService episodeService;
	private final Services services;

	@GetMapping
	@JsonView(EpisodeViews.Public.class)
	public ResponseEntity<?> findAll(@RequestParam(value = "cours", required = false) Optional<Long> cours,
			@RequestParam(value = "visionnePar", required = false) Optional<Long> utilisateur) {

		// get all episodes
		List<EntityModel<Episode>> episodes = StreamSupport
				.stream(episodeService.findAll(cours, utilisateur).spliterator(), false)
				.map(e -> EntityModel.of(e,
						linkTo(methodOn(EpisodeController.class).findOne(null, e.getId())).withSelfRel(),
						linkTo(methodOn(CoursController.class).findOne(e.getCours().getId())).withRel("cours")))
				.collect(Collectors.toList());

		return ResponseEntity.ok(CollectionModel.of(episodes,
				linkTo(methodOn(EpisodeController.class).findAll(null, null)).withSelfRel()));
	}

	@GetMapping("/{id}")
	@JsonView(EpisodeViews.Private.class)
	public ResponseEntity<?> findOne(@RequestHeader("Authorization") String authorization, @PathVariable Long id) {
		Optional<Episode> optionalEpisode = repository.findById(id);
		if (!optionalEpisode.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		Episode episode = optionalEpisode.get();

		if (!authorization.equals("admin")) {
			try {
				// check if utilisateur has cours
				List<Cours> utilisateurCours = coursService.findAll(Optional.empty(), Optional.empty(),
						Optional.of(Long.parseLong(authorization)), Optional.empty());

				if (!utilisateurCours.stream().filter(c -> c.getId().equals(id)).findFirst().isPresent()) {
					return ResponseEntity.status(HttpStatus.PAYMENT_REQUIRED).body("Vous n'êtes pas abonné à ce cours");
				}

				// Add utilisateur episode visionne
				HttpHeaders headers = new HttpHeaders();
				headers.set("Authorization", authorization);
				HttpEntity<?> httpEntity = new HttpEntity<>(headers);

				ResponseEntity<?> response = new RestTemplate().exchange(
						services.getUtilisateurAddress() + "/utilisateurs/{utilisateurID}/episodes/{id}",
						HttpMethod.PUT, httpEntity, String.class, authorization, id);

				if (response.getStatusCode() != HttpStatus.NO_CONTENT) {
					return response;
				}
			} catch (RestClientException e) {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
			}

		}

		return ResponseEntity.ok(EntityModel.of(episode,
				linkTo(methodOn(EpisodeController.class).findOne(authorization, episode.getId())).withSelfRel(),
				linkTo(methodOn(EpisodeController.class).findAll(null, null)).withRel("episodes"),
				linkTo(methodOn(CoursController.class).findOne(episode.getCours().getId())).withRel("cours")));
	}

	@PostMapping
	public ResponseEntity<?> create(@RequestHeader("Authorization") String authorization,
			@RequestBody EpisodeForm episodeForm) {
		try {
			if (!authorization.equals("admin")) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}

			Episode episode = new Episode();
			episode.setNom(episodeForm.getNom());
			episode.setLien(episodeForm.getLien());
			episode.setCours(entityManager.getReference(Cours.class, episodeForm.getCoursID()));

			Episode saved = repository.save(episode);

			EntityModel<Episode> resource = EntityModel.of(saved,
					linkTo(methodOn(EpisodeController.class).findOne(authorization, saved.getId())).withSelfRel());

			return ResponseEntity.created(new URI(resource.getRequiredLink(IanaLinkRelations.SELF).getHref()))
					.body(resource);
		} catch (URISyntaxException e) {
			return ResponseEntity.badRequest().body("Unable to create episode");
		}
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> modify(@RequestHeader("Authorization") String authorization, @PathVariable Long id,
			@RequestBody EpisodeForm episodeForm) {

		if (!authorization.equals("admin")) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

		Optional<Episode> optionalEpisode = repository.findById(id);
		if (!optionalEpisode.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		Episode episode = optionalEpisode.get();

		episode.setNom(episodeForm.getNom());
		episode.setLien(episodeForm.getLien());
		episode.setCours(entityManager.getReference(Cours.class, episodeForm.getCoursID()));

		repository.save(episode);

		return ResponseEntity.noContent().build();
	}
}