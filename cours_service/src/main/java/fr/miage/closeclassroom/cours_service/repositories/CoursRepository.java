package fr.miage.closeclassroom.cours_service.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import fr.miage.closeclassroom.common.models.Cours;

@RepositoryRestResource(exported = false)
public interface CoursRepository extends CrudRepository<Cours, Long>, JpaSpecificationExecutor<Cours> {
}