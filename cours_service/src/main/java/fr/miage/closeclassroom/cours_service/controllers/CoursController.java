package fr.miage.closeclassroom.cours_service.controllers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import fr.miage.closeclassroom.common.Services;
import fr.miage.closeclassroom.common.models.Achat;
import fr.miage.closeclassroom.common.models.Cours;
import fr.miage.closeclassroom.cours_service.repositories.CoursRepository;
import fr.miage.closeclassroom.cours_service.services.CoursService;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping(value = "/cours")
@AllArgsConstructor
public class CoursController {

	private final CoursRepository repository;
	private final CoursService service;
	private final Services services;

	@GetMapping
	public ResponseEntity<?> findAll(@RequestParam(value = "prixMin", required = false) Optional<Float> prixMin,
			@RequestParam(value = "prixMax", required = false) Optional<Float> prixMax,
			@RequestParam(value = "utilisateur", required = false) Optional<Long> utilisateur,
			@RequestParam(value = "theme", required = false) Optional<String> theme) {

		List<EntityModel<Cours>> cours = StreamSupport
				.stream(service.findAll(prixMin, prixMax, utilisateur, theme).spliterator(), false)
				.map(c -> EntityModel.of(c, linkTo(methodOn(CoursController.class).findOne(c.getId())).withSelfRel(),
						linkTo(methodOn(CoursController.class).findEpisodes(c.getId())).withRel("episodes"),
						linkTo(methodOn(CoursController.class).abonnement(null, c.getId(), null)).withRel("abonnement"),
						linkTo(methodOn(CoursController.class).desabonnement(null, c.getId()))
								.withRel("desabonnement")))
				.collect(Collectors.toList());

		return ResponseEntity.ok(CollectionModel.of(cours,
				linkTo(methodOn(CoursController.class).findAll(prixMin, prixMax, utilisateur, theme)).withSelfRel()));
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> findOne(@PathVariable Long id) {
		return repository.findById(id)
				.map(cours -> EntityModel.of(cours,
						linkTo(methodOn(CoursController.class).findOne(cours.getId())).withSelfRel(),
						linkTo(methodOn(CoursController.class).abonnement(null, id, null)).withRel("abonnement"),
						linkTo(methodOn(CoursController.class).desabonnement(null, id)).withRel("desabonnement"),
						linkTo(methodOn(CoursController.class).findEpisodes(id)).withRel("episodes")))
				.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
	}

	@GetMapping("/{id}/episodes")
	public ModelAndView findEpisodes(@PathVariable Long id) {
		return new ModelAndView("redirect:/episodes?cours="+id.toString());
	}

	@PostMapping
	public ResponseEntity<?> create(@RequestHeader("Authorization") String authorization, @RequestBody Cours cours) {
		try {
			if (!authorization.equals("admin")) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Droit administrateur requis");
			}

			Cours saved = repository.save(cours);

			EntityModel<Cours> resource = EntityModel.of(saved,
					linkTo(methodOn(CoursController.class).findOne(saved.getId())).withSelfRel());

			return ResponseEntity.created(new URI(resource.getRequiredLink(IanaLinkRelations.SELF).getHref()))
					.body(resource);
		} catch (URISyntaxException e) {
			return ResponseEntity.badRequest().body("Unable to create " + cours);
		}
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> modify(@RequestHeader("Authorization") String authorization, @PathVariable Long id,
			@RequestBody Cours cours) {
		if (!authorization.equals("admin")) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Droit administrateur requis");
		}

		repository.save(cours);

		return ResponseEntity.noContent().build();
	}

	@PostMapping("/{id}/abonnement")
	public ResponseEntity<?> abonnement(@RequestHeader("Authorization") String authorization, @PathVariable Long id,
			@RequestBody Achat achat) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", authorization);
		HttpEntity<?> httpEntity = new HttpEntity<>(achat, headers);

		return getRestTemplate().postForEntity(
				services.getUtilisateurAddress() + "/utilisateurs/{utilisateurID}/cours/{id}", httpEntity,
				String.class, authorization, id);
	}

	@DeleteMapping("/{id}/abonnement")
	public ResponseEntity<?> desabonnement(@RequestHeader("Authorization") String authorization,
			@PathVariable Long id) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", authorization);
		HttpEntity<?> httpEntity = new HttpEntity<>(headers);

		return getRestTemplate().exchange(services.getUtilisateurAddress() + "/utilisateurs/{utilisateurID}/cours/{id}",
				HttpMethod.DELETE, httpEntity, String.class, authorization, id);
	}

	private RestTemplate getRestTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse response) throws IOException {
				return false;
			}

			@Override
			public void handleError(ClientHttpResponse response) throws IOException {
			}
		});

		return restTemplate;
	}
}